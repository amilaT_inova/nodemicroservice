const express = require('express');
const router = express.Router();

let api= require('./api.v0.js');
let apiuser= require('./api.user.v0.js');


router.use('/api',api); // routing prfix "/api"
router.use('/api/user',apiuser); // routing prfix "/api/user"

router.get('/', (req, res) => {
	 res.send('Test Route bad request');
});	

module.exports=router;