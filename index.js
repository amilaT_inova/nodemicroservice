const express = require('express');
const fs = require('fs');
const yaml = require('js-yaml');
const axios = require('axios');
const Joi = require('@hapi/joi');

//const Request = require("request");
//const cust = require('./customModule1'); import all functions in custom module
//const {getName,getLocation} = require('./customModule1'); // import selected function in custom module

const app = express();
const port = 3000;

let fileContents = fs.readFileSync('./config.yaml', 'utf8');
let data = yaml.safeLoad(fileContents);

app.use(require('./routes')); // default routing start here
	
//console.log(getName());
//console.log(data.SECRET_TOKEN);
app.listen(port, () => console.log(`Example app listening on port ${port}!`));